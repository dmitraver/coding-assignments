package com.esrlabs.codingassign.hashmap;

import org.testng.Assert;
import org.testng.annotations.Test;

public class HashMapTest extends Assert {

    @Test
    public void should_get_inserted_elements() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.add(1, "aaa");
        map.add(2, "bbb");
        map.add(3, "ccc");
        map.add(4, "ddd");

        Assert.assertNotNull(map.get(1));
        Assert.assertNotNull(map.get(2));
        Assert.assertNotNull(map.get(3));
        Assert.assertNotNull(map.get(4));
    }

    @Test
    public void should_expand_and_rehash_table_than_get_inserted_elements() {
        Map<Integer, String> map = new HashMap<Integer, String>(4, 0.5f);
        map.add(1, "aaa");
        map.add(2, "bbb");
        map.add(3, "ccc");
        map.add(4, "ddd");

        Assert.assertNotNull(map.get(1));
        Assert.assertNotNull(map.get(2));
        Assert.assertNotNull(map.get(3));
        Assert.assertNotNull(map.get(4));
    }

    @Test
    public void should_check_that_element_exist_or_not() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.add(1, "aaa");

        Assert.assertTrue(map.contains(1));
        Assert.assertFalse(map.contains(3));
    }

    @Test
    public void should_delete_element() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.add(1, "aaa");

        Assert.assertNotNull(map.get(1));
        Assert.assertTrue(map.remove(1));
        Assert.assertFalse(map.contains(1));
        Assert.assertNull(map.get(1));
    }

}
