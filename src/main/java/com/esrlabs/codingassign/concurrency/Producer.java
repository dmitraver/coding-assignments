package com.esrlabs.codingassign.concurrency;

public class Producer extends Thread {

    private MessageBox messageBox;

    public Producer(MessageBox messageBox) {
        this.messageBox = messageBox;
    }

    @Override
    public void run() {
        int i = 0;
        while (i < 20) {
            messageBox.put("Hello");
            i++;
        }
    }
}
