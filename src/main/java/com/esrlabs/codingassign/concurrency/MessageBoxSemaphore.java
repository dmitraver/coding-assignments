package com.esrlabs.codingassign.concurrency;


import java.util.concurrent.Semaphore;

public class MessageBoxSemaphore implements MessageBox {

    private String message;
    private Semaphore consumerSemaphore = new Semaphore(0);
    private Semaphore producerSemaphore = new Semaphore(1);

    public void put(String message) {
        try {
            producerSemaphore.acquire();
        } catch (InterruptedException e) {
            System.err.println("Producer semaphore waiting is interrupted");
        }
        this.message = message;
        consumerSemaphore.release();
    }

    public String get() {
        try {
            consumerSemaphore.acquire();
        } catch (InterruptedException e) {
            System.err.println("Consumer semaphore waiting is interrupted");
        }

        String message = this.message;
        producerSemaphore.release();
        return message;
    }
}
