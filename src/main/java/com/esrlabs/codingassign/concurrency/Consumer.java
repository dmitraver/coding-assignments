package com.esrlabs.codingassign.concurrency;

public class Consumer extends Thread {

    private MessageBox messageBox;

    public Consumer(MessageBox messageBox) {
        this.messageBox = messageBox;
    }

    @Override
    public void run() {
        int i = 0;
        while (i < 20) {
            String message = messageBox.get();
            System.out.println(message + " World!");
            i++;
        }
    }
}
