package com.esrlabs.codingassign.concurrency;


public class MessageBoxWaitNotify implements MessageBox {

    private String message;
    private boolean available;

    public synchronized void put(String message) {
        while (available) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Producer waiting is interrupted");
            }
        }

        available = true;
        this.message = message;
        notifyAll();
    }

    public synchronized String get() {
        while(!available) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Consumer waiting is interrupted");
            }
        }

        available = false;
        notifyAll();

        return message;
    }
}
