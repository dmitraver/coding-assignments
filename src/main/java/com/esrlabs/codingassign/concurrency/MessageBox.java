package com.esrlabs.codingassign.concurrency;

public interface MessageBox {
    void put(String message);
    String get();
}
