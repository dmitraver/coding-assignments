package com.esrlabs.codingassign.concurrency;


import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MessageBoxLock implements MessageBox {

    private String message;
    private boolean available;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void put(String message) {
        lock.lock();
        try {
            while(available) {
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    System.err.println("Producer is interrupted");
                }
            }

            available = true;
            this.message = message;
            condition.signal();
        } finally {
            lock.unlock();
        }
    }

    public String get() {
        lock.lock();
        try {
            while(!available) {
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    System.err.println("Consumer is interrupted");
                }
            }

            available = false;
            condition.signal();
            return message;
        } finally {
            lock.unlock();
        }
    }
}
