package com.esrlabs.codingassign.concurrency.queue;

import com.esrlabs.codingassign.concurrency.MessageBox;

public class MessageQueueConsumer extends Thread {

    private MessageQueue messageQueue;

    public MessageQueueConsumer(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        int i = 0;
        while (i < 20) {
            Message message = messageQueue.dequeue();
            System.out.println("Consumer get message:" + message);
            i++;
        }
    }
}
