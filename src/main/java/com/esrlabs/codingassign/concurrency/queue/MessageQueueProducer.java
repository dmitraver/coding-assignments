package com.esrlabs.codingassign.concurrency.queue;

import com.esrlabs.codingassign.concurrency.MessageBox;

import java.util.Observable;
import java.util.Random;

public class MessageQueueProducer extends Thread {

    private MessageQueue messageQueue;

    public MessageQueueProducer(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        Random random = new Random();
        int i = 0;
        while (i < 20) {
            Message message = new Message(random.nextInt(20), 1, 2, new Object());
            System.out.println("Producer add message:" + message);
            messageQueue.queue(message);
            i++;
        }
    }
}
