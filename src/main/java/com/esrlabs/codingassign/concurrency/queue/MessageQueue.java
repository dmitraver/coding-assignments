package com.esrlabs.codingassign.concurrency.queue;

import java.util.Iterator;
import java.util.LinkedList;

public class MessageQueue {

    private static final int DEFAULT_MAX_SIZE = 16;
    private LinkedList<Message> messages = new LinkedList<Message>();
    private int size;
    private int messagesNumber;

    public MessageQueue() {
        this(DEFAULT_MAX_SIZE);
    }

    public MessageQueue(int size) {
        if(size <= 0) {
            throw new IllegalArgumentException("Size is less than zero");
        }

        this.size = size;
    }

    public synchronized void queue(Message message) {
        while (isFull()) {
            try {
                System.out.println("Queue is full, producer is waiting...");
                wait();
            } catch (InterruptedException e) {
                System.err.println("Thread waiting on queue is interrupted");
            }
        }

        messagesNumber++;
        messages.add(message);
        notifyAll();
    }

    public synchronized Message dequeue() {
        while (isEmpty()) {
            try {
                System.out.println("Queue is empty, consumer is waiting...");
                wait();
            } catch (InterruptedException e) {
                System.err.println("Thread waiting on dequeue is interrupted");
            }
        }

        messagesNumber--;
        notifyAll();
        return messages.poll();
    }

    public synchronized void deleteAllWithWhatField(int what) {
        Iterator<Message> iterator = messages.iterator();
        while (iterator.hasNext()) {
            Message message = iterator.next();
            if(message.getWhat() == what) {
                iterator.remove();
            }
        }
    }

    private boolean isFull() {
        return messagesNumber == size;
    }

    private boolean isEmpty() {
        return messages.isEmpty();
    }
}
