package com.esrlabs.codingassign.concurrency.queue;

public class Message {

    private int what;
    private int args1;
    private int args2;
    private Object object;

    public Message(int what, int args1, int args2, Object object) {
        this.what = what;
        this.args1 = args1;
        this.args2 = args2;
        this.object = object;
    }

    public int getWhat() {
        return what;
    }

    public int getArgs1() {
        return args1;
    }

    public int getArgs2() {
        return args2;
    }

    public Object getObject() {
        return object;
    }

    @Override
    public String toString() {
        return "Message{" +
                "what=" + what +
                ", args1=" + args1 +
                ", args2=" + args2 +
                ", object=" + object +
                '}';
    }
}
