package com.esrlabs.codingassign;

import com.esrlabs.codingassign.concurrency.*;
import com.esrlabs.codingassign.concurrency.queue.Message;
import com.esrlabs.codingassign.concurrency.queue.MessageQueue;
import com.esrlabs.codingassign.concurrency.queue.MessageQueueConsumer;
import com.esrlabs.codingassign.concurrency.queue.MessageQueueProducer;

public class App {
    public static void main( String[] args ) {
        messageQueueDemo();
    }

    private static void multiThreadedHelloWorldWithWaitNotifyDemo() {
        MessageBox messageBox = new MessageBoxWaitNotify();
        Producer producer = new Producer(messageBox);
        Consumer consumer = new Consumer(messageBox);

        producer.start();
        consumer.start();
    }

    private static void multiThreadedHelloWorldWithLockDemo() {
        MessageBox messageBox = new MessageBoxLock();
        Producer producer = new Producer(messageBox);
        Consumer consumer = new Consumer(messageBox);

        producer.start();
        consumer.start();
    }

    private static void multiThreadedHelloWorldWithSemaphoreDemo() {
        MessageBox messageBox = new MessageBoxSemaphore();
        Producer producer = new Producer(messageBox);
        Consumer consumer = new Consumer(messageBox);

        producer.start();
        consumer.start();
    }

    private static void messageQueueDemo() {
        MessageQueue messageQueue = new MessageQueue(2);
        MessageQueueConsumer consumer = new MessageQueueConsumer(messageQueue);
        MessageQueueProducer producer = new MessageQueueProducer(messageQueue);

        consumer.start();
        producer.start();
    }
}
