package com.esrlabs.codingassign.hashmap;

public interface Map<K, V> {

    void add(K key, V value);

    boolean remove(K key);

    V get(K key);

    boolean contains(K key);
}
