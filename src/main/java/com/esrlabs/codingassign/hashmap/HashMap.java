package com.esrlabs.codingassign.hashmap;

import java.util.TreeMap;

public class HashMap<K, V> implements Map<K, V> {

    private static final int INITIAL_CAPACITY = 16;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private Object[] data;
    private int size;
    private int maxCapacity;
    private int capacity;
    private float loadFactor;

    @SuppressWarnings("unchecked")
    public HashMap() {
        this(INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    @SuppressWarnings("unchecked")
    public HashMap(int size, float loadFactor) {
        if(size < 0) {
            throw new IllegalArgumentException("Size is less than zero");
        }

        if(loadFactor <= 0 || loadFactor >= 2) {
            throw new IllegalArgumentException("Load factor should be in range [0..2]");
        }

        this.size = size;
        this.loadFactor = loadFactor;
        data = new Object[size];
        maxCapacity = (int) (loadFactor * size);
    }

    @SuppressWarnings("unchecked")
    public HashMap(int size) {
        this(size, DEFAULT_LOAD_FACTOR);
    }

    @Override
    public void add(K key, V value) {
        if(capacity >= maxCapacity) {
            increaseAndRehash(size * 2);
        }

        int index = hash(key);
        java.util.Map<K,V> bucket = getBucket(index);
        if(bucket == null) {
            bucket = new TreeMap<K, V>();
            data[index] = bucket;
        }

        bucket.put(key, value);
        capacity++;
    }

    private void increaseAndRehash(int toSize) {
        size = toSize;
        Object[] newData = new Object[size];

        for (int i = 0; i < data.length; i++) {
            java.util.Map<K, V> bucket = getBucket(i);
            if(bucket != null) {
                for (java.util.Map.Entry<K, V> entry : bucket.entrySet()) {
                    int index = hash(entry.getKey());
                    java.util.Map<K, V> b = (java.util.Map<K, V>) newData[index];
                    if(b == null) {
                        b = new TreeMap<K, V>();
                        newData[index] = b;
                    }

                    b.put(entry.getKey(), entry.getValue());
                }
            }
        }

        data = newData;
        maxCapacity = (int) (size * loadFactor);
    }

    @SuppressWarnings("unchecked")
    private java.util.Map<K, V> getBucket(int index) {
        return (java.util.Map<K, V>) data[index];
    }

    @Override
    public boolean remove(K key) {
        int index = hash(key);
        java.util.Map<K,V> bucket = getBucket(index);
        if(bucket != null && bucket.remove(key) != null) {
            capacity--;
            return true;
        }

        return false;
    }

    @Override
    public V get(K key) {
        int index = hash(key);
        java.util.Map<K, V> bucket = getBucket(index);
        if(bucket != null) {
            return bucket.get(key);
        }

        return null;
    }

    @Override
    public boolean contains(K key) {
        int index = hash(key);
        java.util.Map<K, V> bucket = getBucket(index);
        return bucket != null && bucket.containsKey(key);
    }

    private int hash(K key) {
        return key.hashCode() % size;
    }

    public void print() {
        for (int i = 0; i < data.length; i++) {
            java.util.Map<K, V> bucket = getBucket(i);
            if(bucket != null) {
                for(java.util.Map.Entry<K, V> entry : bucket.entrySet()) {
                    System.out.print(entry.getValue() + " ");
                }
                System.out.println();
            }
        }
    }
}
