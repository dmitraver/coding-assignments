package com.esrlabs.codingassign.tree;

public class BinarySearchTree<K extends Comparable<K>, V> {

    public static class Node<K, V> {
        public K key;
        public V value;

        public Node<K,V> left;
        public Node<K,V> right;
    }

    private Node<K,V> root;

    public void add(K key, V value) {
        root = add(root, key, value);
    }

    private Node<K, V> add(Node<K, V> root, K key, V value) {
        if(root == null) {
            root = new Node<K, V>();
            root.key = key;
            root.value = value;
        } else {
            if(key.compareTo(root.key) < 0) {
                root.left = add(root.left, key, value);
            } else {
                root.right = add(root.right, key, value);
            }
        }

        return root;
    }

    public V get(K key) {
        return get(root, key);
    }

    private V get(Node<K, V> root, K key) {
        if(root == null) {
            return null;
        } else {
            if(root.key.equals(key)) {
                return root.value;
            } else if(key.compareTo(root.key) < 0) {
                return get(root.left, key);
            } else {
                return get(root.right, key);
            }
        }
    }

    public void printInOrder() {
        printInOrder(root);
    }

    private void printInOrder(Node<K, V> root) {
        if(root != null) {
            printInOrder(root.left);
            System.out.println(root.key + " - " + root.value);
            printInOrder(root.right);
        }
    }

    public void remove(K key) {
        root = remove(root, key);
    }

    private Node<K, V> remove(Node<K, V> root, K key) {
        if(root == null) {
            return null;
        } else if(key.compareTo(root.key) < 0) {
            root.left = remove(root.left, key);
        } else if(key.compareTo(root.key) > 0) {
            root.right = remove(root.right, key);
        } else {
            if(root.left != null && root.right != null) {
                Node<K, V> maxNode = findMax(root.left);
                root.key = maxNode.key;
                root.value = maxNode.value;
                root.left = remove(root.left, maxNode.key);
            } else {
                if(root.left == null) {
                    root = root.right;
                } else {
                    root = root.left;
                }
            }
        }

        return root;
    }

    private Node<K, V> findMax(Node<K, V> root) {
        if(root != null) {
            Node<K, V> current = root;
            while(current.right != null) {
                current = current.right;
            }

            return current;
        }

        return null;
    }

    public boolean contains(K key) {
        V value = get(key);
        return value != null;
    }

}
